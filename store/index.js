import axios from 'axios'
axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest'
const cookieparser = process.server ? require('cookieparser') : undefined
import $ from 'jquery'

export const state = () => ({
    auth: {
        usuario: {},
        token: null,
        logueado: false
    },
    bienvenida: 0,
    ultimolink: "/",
    horariosPresencial: [],
    horariosEvento: [],
    filtroHorarios: [],
    filtroHorariosEvento: [],
    clasesPresencial: [],
    cantidadesPresencial: [],
    clasesOnline: [],
    cantidadesOnline: [],
    archivos: [],
    filtroArchivos: [],
    locales: [],
    filtroLocales: [],
    articulos: [],
    productos: [],
    ubigeo: [],
    distritos: [],
    calculador: [],
    calculos: [],
    solucion: [],
    materiales: [],
    beneficios: [],
    marcadores: [],
    renderMap: true,
    slides: null,
    intento: 0,
    examen: [],
    idExamen: 0,
    dias : ["Lunes","Martes","Miercoles","Jueves","Viernes","Sábado","Domingo"],
    meses : ["","Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"],
    dia: '',
    mes: '',
    dianombre: '',
    alerta: {
        titulo: 'hola',
        contenido: 'como estas',
        botones: []
    },
    filtros: [],
    instalar: true
})

export const mutations = {
    setAuth (state, auth) {
        if(auth == null) {
            state.auth = {
                usuario: {},
                token: null,
                logueado: false
            }
        }
        else {
            state.auth = auth
        }
    },
    bienvenida(state, value) {
        state.bienvenida = value
    },
    llenarHorarios(state, value){
        var presencial = []
        var evento = []
        value.forEach(element => {
            if(element.nvl_id == 4) {
                evento.push(element)
            }
            else {
                presencial.push(element)
            }
        });
        state.horariosPresencial = presencial
        state.filtroHorarios = presencial
        state.horariosEvento = evento
        state.filtroHorariosEvento = evento
    },
    llenarFiltroHorarios(state, value) {
        state.filtroHorarios = value
    },
    llenarFiltroHorariosEvento(state, value) {
        state.filtroHorariosEvento = value
    },
    actualizarHorariosPresencial(state, value){
        var evento = 0
        state.horariosPresencial.forEach(element => {
            if(element.hrr_id == value.id) {
                element.horarioinscrito = value.estado
                evento = 1
            }
        });
        if(evento != 1){
            state.horariosEvento.forEach(element => {
                if(element.hrr_id == value.id) {
                    element.horarioinscrito = value.estado
                }
            });
        }
    },

    llenarClasesPresencial(state, value){
        value.forEach(clase => {
            state.horariosPresencial.forEach(element => {
                if(clase.cls_id == element.cls_id) {
                    clase.activo = 1
                    clase.link = element.hrr_id
                }
            });
        });
        var cantidades = [
            { completado: 0, total: 0 },
            { completado: 0, total: 0 },
            { completado: 0, total: 0 }
        ]
        value.forEach(element => {
                if(element.nvl_id == 1) {
                    cantidades[0].total++
                    if(element.completo == '1') {
                        cantidades[0].completado++
                    }
                }
                if(element.nvl_id == 2) {
                    cantidades[1].total++
                    if(element.completo == '1') {
                        cantidades[1].completado++
                    }
                }
                if(element.nvl_id == 3) {
                    cantidades[2].total++
                    if(element.completo == '1') {
                        cantidades[2].completado++
                    }
                }
        })
        state.cantidadesPresencial = cantidades
        state.clasesPresencial = value
    },

    llenarClasesOnline(state, value){
        var cantidades = [
            { completado: 0, total: 0 }
        ]
        value.forEach(element => {
            if(element.nvl_id == 1) {
                cantidades[0].total++
            }
            if(element.completo == 1) {
                cantidades[0].completado++
            }   
        })
        state.cantidadesOnline = cantidades
        state.clasesOnline = value
    },
    actualizarClasesOnline(state, value){
        var cantidad = 0
        state.clasesOnline.forEach(element => {
            if(element.cls_id == value.clase) {
                element.completo = value.aprobado
                if(value.nota > element.int_puntuacion){
                    element.int_puntuacion = value.nota
                }
            }
            if(element.completo == 1) {
                cantidad++
            }  
        });
        state.cantidadesOnline.completado = cantidad
    },
    llenarArchivos(state, value){
        var nuevo = []
        value.forEach(element => {
            nuevo.push(element)
        });
        nuevo.sort(function (a, b) {       
            if (a.dcm_creacion > b.dcm_creacion) {
                return 1;
            }
            if (a.dcm_creacion < b.dcm_creacion) {
                return -1;
            }
            return 0;
        });
        state.archivos = nuevo
    },
    llenarFiltroArchivos(state, value){
        var nuevo = []
        value.forEach(element => {
            nuevo.push(element)
        });
        nuevo.sort(function (a, b) {       
            if (a.dcm_titulo > b.dcm_titulo) {
                return 1;
            }
            if (a.dcm_titulo < b.dcm_titulo) {
                return -1;
            }
            return 0;
        });
        state.filtroArchivos = nuevo
    },
    ordenarArchivos(state, value) {
        var data = state.filtroArchivos
        if (value == "recomendado"){
            data.sort(function (a, b) {       
                if (a.dcm_calificacion > b.dcm_calificacion) {
                    return -1;
                }
                if (a.dcm_calificacion < b.dcm_calificacion) {
                    return 1;
                }
                return 0;
            });
        }
        if (value == "descarga"){
            data.sort(function (a, b) {
                if (parseInt(a.dsc_count) > parseInt(b.dsc_count)) {
                    return -1;
                }
                if (parseInt(a.dsc_count) < parseInt(b.dsc_count)) {
                    return 1;
                }
                return 0;
            });
        }
        state.filtroArchivos = data
    },
    llenarLocales(state, value){
        state.locales = value
    },
    llenarFiltroLocales(state, value){
        state.filtroLocales = value
        var dato = []
        value.forEach(element => {
            var coordenadas = element.coordenadas
            var arraylist = coordenadas.split(",")
            var lat = Number(arraylist[0])
            var lng = Number(arraylist[1])
            var position =  {lat: lat, lng: lng} 
            dato.push(position)
        });
        state.marcadores = dato
    },
    renderMap(state, value){
        state.renderMap = value
    },
    llenarArticulos(state, value){
        state.articulos = value
    },
    llenarProductos(state, value){
        state.productos = value
    },
    llenarUbigeo(state, value){
        state.ubigeo = value
        var distritos = []
        value.forEach(element => {
            distritos.push(element.label)
        })
        state.distritos = distritos
    },
    llenarCalculador(state, value){
        state.calculador = value
    },
    llenarCalculos(state, value){
        state.calculos = value
    },
    llenarSolucion(state, value){
        state.solucion = value
    },
    llenarMateriales(state, value){
        state.materiales = value
    },
    llenarBeneficios(state, value){
        state.beneficios = value
    },
    llenarSlides(state, value){
        state.slides = value.slides
        state.intento = value.intento
    },
    llenarExamen(state, value){
        state.examen = value
    },
    llenarExamenId(state, value){
        state.idExamen = value
    },
    agregarFecha(state, value){
        value.forEach(element => {
            var fecha = String(element.hrr_fecha)
            var dia = fecha.slice(-2)
            var mes = state.meses[Number(fecha.slice(-5,-3))]
            var date = new Date(element.hrr_fecha)
            var dianombre = state.dias[date.getDay()]
            element.dia = dia
            element.mes = mes
            element.dianombre = dianombre
        });
    },
    agregarFechaArticulo(state, value){
        value.forEach(element => {
            var fecha = String(element.ttr_creacion)
            var dia = fecha.slice(-11,-9)
            var mes = state.meses[Number(fecha.slice(-14,-12))]
            var date = new Date(element.ttr_creacion)
            var dianombre = state.dias[date.getDay()]
            element.dia = dia
            element.mes = mes
            element.dianombre = dianombre
        });
    },
    cambiarAlerta(state, value){
        state.alerta.titulo = value.titulo
        state.alerta.contenido = value.contenido
        state.alerta.botones = value.botones
    },
    cambiarLink(state, value){
        state.ultimolink = value
    },
    instalar(state){
        state.instalar = false
    }
}

export const actions = {
    async nuxtServerInit ({ commit, dispatch }, { req }) {
        let auth = null
        if (req.headers.cookie) {
            
        const parsed = cookieparser.parse(req.headers.cookie)
            try {
                auth = JSON.parse(parsed.auth)
            } catch (err) {
                // No valid cookie found
            }
        }
        commit('setAuth', auth)

        /*await dispatch('getHorarios')*/
        await dispatch('getClasesOnline')
        await dispatch('getArchivos')
        await dispatch('getClasesPresencial')
        /*await dispatch('getSlides')*/
        /*await dispatch('getExamen')*/
        await dispatch('getLocales')
        await dispatch('getArticulos')
        await dispatch('getProductos')
        await dispatch('getUbigeo')
        await dispatch('getCalculador')
        await dispatch('getCalculos')
        await dispatch('getSolucion')
        await dispatch('getMateriales')
        await dispatch('getBeneficios')
    },
    async getHorarios({commit}){
        await axios.get(`${process.env.BASE_URL}horarios?token=${this.state.auth.token}`)
        .then(res => {
            const value = res.data.horarios;
            commit('agregarFecha', value)
            commit('llenarHorarios', value)
            //console.log(res.data)
        })
    },
    async getClasesPresencial({commit}){
        await axios.get(`${process.env.BASE_URL}clases/presencial?token=${this.state.auth.token}`)
        .then(res => {
            const value = res.data.presencial;
            commit('llenarClasesPresencial', value)
            //console.log(res.data.presencial)
        })
    },
    async getClasesOnline({commit}){
        await axios.get(`${process.env.BASE_URL}clases/online?token=${this.state.auth.token}`)
        .then(res => {
            commit('llenarClasesOnline', res.data.online)
            //console.log(res.data.online)
        })
    },
    async getArchivos({commit}){
        await axios.get(`${process.env.BASE_URL}archivos`)
        .then(res => {
            var value = res.data.archivos
            commit('llenarArchivos', value)
            commit('llenarFiltroArchivos', value)
        })
    },
    async getLocales({commit}){
        await axios.get(`${process.env.BASE_URL}locales`)
        .then(res => {
            commit('llenarLocales', res.data.locales)
            commit('llenarFiltroLocales', res.data.locales)
        })
    },
    async getArticulos({commit}){
        await axios.get(`${process.env.BASE_URL}articulos`)
        .then(res => {
            commit('agregarFechaArticulo', res.data.articulos)
            commit('llenarArticulos', res.data.articulos)
        })
    },
    async getProductos({commit}){
        await axios.get(`${process.env.BASE_URL}productos.json`)
        .then(res => {
            commit('llenarProductos', res.data)
        })
    },
    async getUbigeo({commit}){
        await axios.get(`${process.env.BASE_URL}ubigeo.json`)
        .then(res => {
            commit('llenarUbigeo', res.data)
        })
    },
    async getCalculador({commit}){
        await axios.get(`${process.env.BASE_URL}calculador`)
        .then(res => {
            commit('llenarCalculador', res.data)
        })
    },
    async getCalculos({commit}){
        await axios.get(`${process.env.BASE_URL}calculador/calculos?token=${this.state.auth.token}`)
        .then(res => {
            commit('llenarCalculos', res.data.calculos)
        })
    },
    async getSolucion({commit}){
        await axios.get(`${process.env.BASE_URL}calculador/soluciones`)
        .then(res => {
            commit('llenarSolucion', res.data.solucion)
        })
    },
    async getMateriales({commit}){
        await axios.get(`${process.env.BASE_URL}calculador/final`)
        .then(res => {
            commit('llenarMateriales', res.data)
        })
    },
    async getBeneficios({commit}){
        await axios.get(`${process.env.BASE_URL}beneficios`)
        .then(res => {
            commit('llenarBeneficios', res.data.beneficios)
        })
    },
    async getSlides({commit}, value){
        await axios.get(`${process.env.BASE_URL}slides?token=${this.state.auth.token}&slug=${value}`)
        .then(res => {
            commit('llenarSlides', res.data)
            commit('llenarExamen', res.data.preguntas)
        })
    },
    async hacerInscripcion({commit}, value){
        var alerta = {}
        await axios.post(`${process.env.BASE_URL}matriculas/hacer?token=${this.state.auth.token}`, {id: value})
        .then(res => {
            console.log(res.data)
            if(res.data.tipo == "error"){
                alerta.titulo = "Parece que hubo un error"
                alerta.contenido = res.data.respuesta
                alerta.botones = []
            }
            else {
                alerta.titulo = "Felicitaciones"
                alerta.contenido = "Se inscribió a este curso correctamente"
                alerta.botones = []
                var data = {id: value, estado: 1}
                commit('actualizarHorariosPresencial', data)
            }
            
            //console.log(res.data.archivos)
        })
        commit('cambiarAlerta', alerta)
    },
    async quitarInscripcion({commit}, value){
        await axios.post(`${process.env.BASE_URL}matriculas/quitar?token=${this.state.auth.token}`, {id: value})
        .then(res => {
            var alerta = {}
                alerta.titulo = "¡Listo!"
                alerta.contenido = "Quitaste tu inscripciòn"
                alerta.botones = []
                commit('cambiarAlerta', alerta)
                var data = {id: value, estado: 0}
                commit('actualizarHorariosPresencial', data)
        })
    }
}

