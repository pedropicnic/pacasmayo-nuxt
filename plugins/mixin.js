import Vue from 'vue'
import axios from 'axios'
axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest'
import $ from 'jquery'

Vue.mixin({
  methods: {
    getAnio(value) {
        var fecha = String(value)
        var anio = fecha.slice(0,4)
        return anio
    },
    validarCorreo(email) {
      var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
      return re.test(email);
    },
    formatoHora(h) {
          var num = String(h);
          var time= num.slice(0,5);
          time = time.toString ().match (/^([01]\d|2[0-3])(:)([0-5]\d)(:[0-5]\d)?$/) || [time];
          if (time.length > 1) { // If time format correct
              time = time.slice (1);  // Remove full string match value
              time[5] = +time[0] < 12 ? ' am' : ' pm'; // Set AM/PM
              time[0] = +time[0] % 12 || 12; // Adjust hours
          }
          var hora = time.join ('');
        return hora
    },
    nivel(n){
      if(n == 1){
        return "verde"
      }
      else {
        if(n == 2){
          return "rojo"
        }
        else {
          if(n == 3){
            return "negro"
          }
          else {
              return "azul"
          }
        }
      }
    },
    Imagen(imagen) {
        return `${process.env.FILE_URL}${imagen}`
    },
    mostrarMenu() {
      $(".contenedor-menu").css("width","100%")
      $(".contenedor-menu").css("transition-duration","0.2s").css("transform","translateX(0)")
    },
    cerrarMenu() {
      $(".contenedor-menu").css("width","300px")
      $(".contenedor-menu").css("transition-duration","0.2s").css("transform","translateX(-280px)")
    },
    mostrarContenedorDebajo(){
      $(".contenedor").css("overflow-y","hidden").css("height","100vh")
      $(".info-interna").css("transition-delay",".2s").css("transition-duration",".4s").css("bottom", "0px").css("opacity",1)
      $(".descripcion .capa").css("transition-duration",".1s").css("height","100vh").css("opacity",.2)
    },
    mostrarAlerta(){
      $("#alerta").addClass("activo")
    },
    cerrarAlerta(){
      $("#alerta").removeClass("activo")
    },
    mostrarFiltro() {
      //document.getElementById("filtro").style.display = "block";
      $("#filtro").addClass("activo")
      $("body").css("overflow","hidden")
    },
    cerrarFiltro() {
      $("#filtro").removeClass("activo")
      $("body").css("overflow","visible")
    },
    mostrarLocal() {
      $("#local").addClass("activo")
    },
    cerrarLocal() {
      $("#local").removeClass("activo")
    },
    mostrarProducto() {
      $("#producto").addClass("activo")
    },
    cerrarProducto() {
      $("#producto").removeClass("activo")
    },
    cambiarFiltroActivo(slug) {
        let oldActive = $("#filters .activo").first();
        if (slug !== oldActive.data("filter")) {
          $("#filters .filtro").removeClass("activo");
          $("#filters .filtro."+slug).addClass("activo");
        }
    },
    mostrarCargador(){
      $("#cargador").addClass("activo")
    },
    cerrarCargador(){
      $("#cargador").removeClass("activo")
    },
    mostrarPopup(){
      $("#popup").addClass("activo")
    },
    cerrarPopup(){
      this.$store.commit('bienvenida', 0)
      $("#popup").removeClass("activo")
    },
    cortarTexto(value){
      if(value.length > 50) {
          value = value.substr(0,50)+"...";
      }
      return value
    },
    actualizarInfo(){
      /*this.$store.dispatch('getHorarios')*/
      this.$store.dispatch('getClasesOnline')
      this.$store.dispatch('getClasesPresencial')
      this.$store.dispatch('getCalculos')
    },
    alerta_inscribir(value){
      var alerta = {
          titulo: "¿Quieres inscribirte en esta clase?",
          contenido: "",
          botones: [
              {texto: "SI", color: "verde", link: null, evento: "inscribir", id: value},
              {texto: "NO", color: "rojo", link: null, evento: "cerrar"}
          ]
      }
      this.$store.commit('cambiarAlerta', alerta)
      this.mostrarAlerta()
    },
    alerta_desinscribir(value){
      var alerta = {
          titulo: "¿Estás seguro que quieres quitar tu inscripción?",
          contenido: "",
          botones: [
              {texto: "SI", color: "verde", link: null, evento: "desinscribir", id: value},
              {texto: "NO", color: "rojo", link: null, evento: "cerrar"}
          ]
      }
      this.$store.commit('cambiarAlerta', alerta)
      this.mostrarAlerta()
    },
    alerta_logueado(value){
      var alerta = {
          titulo: "Aviso",
          contenido: "Debes haber iniciado sesión para ver este contenido.",
          botones: [
              {texto: "INGRESAR", color: "verde", link: "/ingresar", evento: null},
          ]
      }
      this.$store.commit('cambiarLink', value)
      this.$store.commit('cambiarAlerta', alerta)
      this.mostrarAlerta()
    },
    requisitoOnline(value){
      var requisito = []
      var contar = 0
      var clase = ''
      this.$store.state.clasesOnline.forEach(element => {
        if(element.cls_alias == value && element.cls_requisito != ""){
          requisito = element.cls_requisito.split(",");
        }
      });
      this.$store.state.clasesOnline.forEach(element => {
        $.each(requisito, function(index, value) {
          if(element.cls_id == value){
            if(element.completo == 1){
              contar++
            }
            else {
              clase = clase + "<br>-" + element.cls_nombre 
            }
          }
        });
      });
      var req = {}
      if(requisito.length == contar){
        req.res = true
      }
      else {
        req.res = false
        req.clase = clase
      }
      return req
    }
  }
})