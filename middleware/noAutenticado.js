export default function ({ store, redirect }) {
    // If the user is authenticated redirect to home page
    console.log(!store.state.auth.logueado)
    if (store.state.auth.logueado) {
      return redirect('/')
    }
  }