require('dotenv').config()
import axios from 'axios'
export default {
  mode: 'universal',
  serverMiddleware: ["redirect-ssl"],
  /*
  ** Headers of the page
  title: process.env.npm_package_name || '',
  */
  head: {
    title: 'Constructores del Futuro',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=0' },
      { hid: 'description', name: 'description', content: 'Conviertete en un experto en Sistema Drywall. Únete el Club y recibe capacitaciones gratuitas, descuentos y beneficios exclusivos para miembros.' },
      { name: 'google-site-verification', content: 'j2m6aWPh-bDzBJdGVlEUKn7ZFI_E2uuC-9Nxh9CEqiQ'}
    ],
    link: [
      { rel: "stylesheet", href: "https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css", 
      integrity: "sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" , crossorigin: "anonymous"},
      { rel: 'stylesheet', href: 'https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css'},
      { rel: 'stylesheet', href: 'https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.theme.default.min.css'},
      { rel: 'stylesheet', href: 'https://use.fontawesome.com/releases/v5.5.0/css/all.css', integrity: 'sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU', crossorigin: "anonymous"}
    ]
  },
  auth: {
    redirect: {
      callback:'/callback',
      home: '/validar'
    },
    strategies: {
      facebook: {
        client_id: '228459548422983',
        userinfo_endpoint: 'https://graph.facebook.com/v2.12/me?fields=about,first_name,last_name,email',
        scope: ['public_profile', 'email'],
      },
      google: {
        client_id: '120849980630-ecfm8v18m8nvlordvdf05mcc40uftvn8.apps.googleusercontent.com'
      }
    }
  },
  /*
  ** Customize the progress-bar color
  */
  loading: { color: '#fff' },
  /*
  ** Global CSS
  */
  css: [
    '@/assets/css/style.scss'
  ],
 
  /*
  ** Plugins to load before mounting the App
  */
  plugins: [
    {src: '@/plugins/owl.js', ssr: false}, // Only works on client side
    {src: '@/plugins/mixin.js'}, // Only works on client side
    {src: "@/plugins/isotope.js", ssr: false },
    {src: "@/plugins/sweetalert.js", ssr: false },
    {src: "@/plugins/html2canvas.js", ssr: false },
    {src: "@/plugins/youtube", ssr: false},
    {src: "@/plugins/gtm.js", ssr: false},
    {src: "@/plugins/installer.js", ssr: false},
  ],
  /*
  ** Nuxt.js dev-modules
  */
  buildModules: [
   
  ],

  /*
  ** Nuxt.js modules
  */

  env: {
    VUE_APP_GOOGLE_MAPS_API_KEY: process.env.VUE_APP_GOOGLE_MAPS_API_KEY,
    BASE_URL: process.env.BASE_URL,
    FILE_URL: process.env.FILE_URL
  },
  modules: [
    // Doc: https://axios.nuxtjs.org/usage
    '@nuxtjs/axios',
    '@nuxtjs/auth',
    '@nuxtjs/pwa',
    'nuxt-lazy-load',
    '@nuxtjs/firebase',
    '@nuxtjs/gtm',
    '@nuxtjs/onesignal',
    'nuxt-leaflet',
    '@nuxtjs/robots',
    '@nuxtjs/sitemap'
  ],
  robots: {
    UserAgent: '*',
    Allow: ['/articulos','/calculadora','/archivos','/donde-comprar'] // accepts function
  },
  sitemap: {
    defaults: {
      priority: 1,
      lastmod: '2020-05-27T15:47:06+00:00',
    },
    exclude: [
      '/beneficios','/callback','/eventos','/eventos/**','/perfil/','/perfil/**','/productos','/validar','/capacitaciones/presenciales'
    ],
    routes: async () => {
      let base = process.env.BASE_URL
      const { data } = await axios.get(`${base}articulos`)
      return data.articulos.map(v => `/articulos/${v.ttr_alias}`)
    }
  },
  firebase: {
    config: {
      apiKey: 'AIzaSyB5zdm9qGdfbaq4qi0YUbMedRbccnxpTRs',
      authDomain: 'http://cdf-test-14e5e.firebaseapp.com/',
      databaseURL: 'https://cdf-test-14e5e.firebaseio.com/',
      projectId: 'cdf-test-14e5e',
      storageBucket: 'http://cdf-test-14e5e.appspot.com/',
      messagingSenderId: '15636542898',
      appId: '1:15636542898:web:aa37fd2ce8bc5fb06ba0ab',
      measurementId: 'G-9LYTXSF7EJ'
    },
    services: {
      analytics: true
    }
  },
  /*    'nuxt-lazy-load'*/
  /*
  ** Axios module configuration
  ** See https://axios.nuxtjs.org/options
  */
  axios: {
  },

  router:{
  },
  /*
  ** Build configuration
  */
  build: {
    /*
    ** You can extend webpack config here
    */
   extend(config, ctx) {
    
    }
  },
  pwa: {
    manifest: {
      name: 'Constructores del Futuro',
      lang: 'es'
    }
  },
  oneSignal: {
    init: {
      appId: 'f4d8401f-9be4-4b45-b947-6c740fb0e072',
      allowLocalhostAsSecureOrigin: true,
      welcomeNotification: {
          disable: true
      }
    }
  }
}
